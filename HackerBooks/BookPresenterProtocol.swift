//
//  BookingPresenterProtocolOutput.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 28/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

protocol BookPresenterInput{
    
}

protocol BookPresenterOutput{
   
    func numberOfRowInSection(section:Int)->Int
}

