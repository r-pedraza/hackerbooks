//
//  BookImageCache.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 27/1/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

struct CacheEntry {
    let URL: NSURL
    let image: UIImage
}

struct Cache {
    
    let CacheSize: Int = 8
    var cache:[CacheEntry?]
    
    init(){
        self.cache = [CacheEntry?](count:CacheSize, repeatedValue:nil)
    }
    
    func indexForKey(key: NSURL) -> Int {
        let index = abs(key.hash) % CacheSize
        return index
    }
    
    mutating func write(entry: CacheEntry){
        cache[ indexForKey(entry.URL) ] = entry
    }
    
    func read(key: NSURL) -> CacheEntry? {
        let entry = cache[ indexForKey(key) ]
        let isCached = (entry != nil) && entry!.URL.absoluteString == key.absoluteString
        if (!isCached){
        }
        return isCached ? entry : nil
    }
    
}
