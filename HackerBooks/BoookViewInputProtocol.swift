//
//  BoookViewInputProtocol.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit


protocol BookViewInputProtocol {

    func giveMeListFromBooks(books:Dictionary<String,Array<BookModel>>)
}

protocol BookViewOutputProtocol {
    
}