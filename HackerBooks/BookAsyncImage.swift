//
//  BookAsyncImage.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 4/1/16.
//  Copyright © 2016 Raúl Pedraza. All rights reserved.
//

import UIKit

struct BooksAsyncImage {
    
    static var cache = Cache()
    
    static let downloadQueue = dispatch_queue_create("book", DISPATCH_QUEUE_CONCURRENT)
    let semaphore:dispatch_semaphore_t = dispatch_semaphore_create(2)
    
    func asyncloadBookImage(book: BookModel, completion:(UIImage)->())
    {
        
            let cacheEntry = BooksAsyncImage.cache.read(book.bookImage!)
            
            if cacheEntry != nil {
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    completion(cacheEntry!.image)
                })
                
            } else {
                
                if (dispatch_semaphore_wait(self.semaphore, DISPATCH_TIME_NOW) == 0) {
                    dispatch_async(BooksAsyncImage.downloadQueue){
                        let data = NSData(contentsOfURL:book.bookImage!)
                        if data != nil {
                            let image = UIImage().imageResizeImage(UIImage(data: data!)!, newWidth: 100.0)
                            BooksAsyncImage.cache.write(CacheEntry(URL:book.bookImage!, image: image))
                            dispatch_async(dispatch_get_main_queue(), { () -> Void in
                                completion(image)
                            })
                        }
                        dispatch_semaphore_signal(self.semaphore)
                    }
                } else {
                }
            }
        }
    
     }

