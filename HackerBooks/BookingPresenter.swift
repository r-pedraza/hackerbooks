//
//  BookPresenter.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BookPresenter:BookPresenterOutput,BookPresenterInput,BookInteractorOutput{
    
    var view      :BooksTableViewController?
    var interactor:BookInteractor?
    var routing   :BookRouting?
    let HEIGTH_FOR_ROW:Float = 200.0

   
    func booksWithDictionary(books:Dictionary<String,Array<BookModel>>){
        view?.giveMeListFromBooks(books)
    }
    
    //MARK - UITableDataSource
    
    func numberOfCells()-> Int{
     return (self.interactor?.bookModels().count)!
    }
    
    func heightForRow()-> CGFloat{
    return CGFloat(self.HEIGTH_FOR_ROW)
    }
    
    func numberOfSections()-> Int{
        return (self.interactor?.bookTags().count)!
    }
    
    func numberOfRowInSection(section:Int)-> Int{
      return  (self.interactor?.numberOfRowInSection(section))!
    }
    
    func titleForSection(section:Int)-> String{
     return (self.interactor?.bookTags()[section])!
    }
    
    //MARK- INPUT
    
    func cellForBook(tableView:UITableView, indexPath:NSIndexPath,books:Dictionary<String,Array<BookModel>>)-> BookCell{
        
    var key = books[(self.interactor?.bookTags()[indexPath.section])!]//tag
        let book = key![indexPath.row]//book

     return  BookCell().rowWithBookModel(book, tableView: tableView)
    
    }
}


