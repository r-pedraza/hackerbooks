//
//  JSONProcesing.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 29/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit


//MARK - Keys

enum JSONKeys:String{
    
    case authors  = "authors"
    case imageURL = "image_url"
    case pdfURL   = "pdf_url"
    case tags     = "tags"
    case title    = "title"
}

//MARK - Aliases

typealias JSONObject     = AnyObject
typealias JSONDictionary = [String:JSONObject]
typealias JSONArray      = [JSONDictionary]

//MARK: Errors

enum JSONProcessingError:ErrorType{
    
    case WrongURLFormatForJSONResource
    case ResourcePointedByURLNotReachable
    case JSONParsingError
    case WrongJSONFormat
}

//MARK - Decoding

func decode(bookingModel json: JSONDictionary) throws -> BookModel{
    
    guard let imageURLString = json[JSONKeys.imageURL.rawValue] as? String,
        
        imageURL = NSURL(string: imageURLString)else{
            
            throw JSONProcessingError.ResourcePointedByURLNotReachable
    }
    guard let pdfURLString = json[JSONKeys.pdfURL.rawValue] as? String,
        
        pdfURL = NSURL(string: pdfURLString)else{
            
            throw JSONProcessingError.ResourcePointedByURLNotReachable
    }
 
    let authors  = json[JSONKeys.authors.rawValue]  as! String
    let tags     = json[JSONKeys.tags.rawValue]     as! String
    let title    = json[JSONKeys.title.rawValue]    as! String


    return BookModel(
        title : title,
        author: authors,
        tags  : tags,
        image : imageURL,
        pdf   : pdfURL)
}