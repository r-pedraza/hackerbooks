//
//  HackerBooksRouting.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BookRouting {
    
    
    let vc:BooksTableViewController = BooksTableViewController()
    let presenter:BookPresenter     = BookPresenter()
    let interactor:BookInteractor   = BookInteractor()
    var navigationController: UINavigationController?

    init(){
        vc.presenter         = presenter;
        presenter.view       = vc;
        presenter.interactor = interactor;
        presenter.routing    = self;
        interactor.presenter = presenter;
        navigationController = UINavigationController(rootViewController: vc)
        
        self.presenter.booksWithDictionary(self.interactor.viewModelFromModel(self.interactor.bookModels()));
    }
}