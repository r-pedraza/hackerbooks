//
//  BooksDao.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import Foundation


class BookDao {
    
    var books:NSArray = NSArray();
    
    func json()->NSArray{
        
        if let path = NSBundle.mainBundle().pathForResource("books_readable", ofType: "json") {
            do {
                let jsonData = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
                do {
                    self.books = try NSJSONSerialization.JSONObjectWithData(jsonData, options: NSJSONReadingOptions.MutableContainers) as! NSArray
                                        
                } catch {}
            } catch {}
        }
        
        return self.books;
    }
    
}


