//
//  BookModel.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BookModel {

    var bookTitle:String?
    var bookAuthor:String?
    var bookTags:String?
    var bookImage:NSURL?
    var bookPdf:NSURL?
    
    init(booksFromJson:NSDictionary){
    
        self.bookTitle  = booksFromJson .valueForKey("title")     as? String
        self.bookAuthor = booksFromJson .valueForKey("authors")   as? String
        self.bookTags   = booksFromJson .valueForKey("tags")      as? String
        self.bookImage  = booksFromJson .valueForKey("image_url") as? NSURL
        self.bookPdf    = booksFromJson .valueForKey("pdf_url")   as? NSURL
    }
    
    init(title:String,author:String,tags:String,image:NSURL,pdf:NSURL){
    
        self.bookTitle  = title
        self.bookAuthor = author;
        self.bookTags   = tags
        self.bookImage  = image
        self.bookPdf    = pdf
    }
    
}
