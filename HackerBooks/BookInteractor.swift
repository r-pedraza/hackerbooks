//
//  BookInteractor.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BookInteractor:BookInteractorOutput,BookPresenterOutput{
  
    var presenter:BookPresenter?
    let bookDao: BookDao
    var books:Dictionary<String,Array<BookModel>> = [:]
    var asynImage:BooksAsyncImage?
    var tags:Array<String> = []
    
    init(){
        
        self.bookDao = BookDao()
        self.books = self.viewModelFromModel(self.bookModels())
    }
    
    
    //MARK - BookPresenterOutput
    func booksWithDictionary(books:Dictionary<String,Array<BookModel>>){
        self.presenter?.booksWithDictionary(books)
    }
    
    func bookModels() -> [BookModel] {
        
        let booksJSON = self.bookDao.json()
        var objects:[BookModel] = []
        for var i = 0;i < booksJSON.count;i++
        {
            do{
                let book:BookModel = try decode(bookingModel: booksJSON[i] as! JSONDictionary)
                objects.append(book)
            
            }catch{
                
                JSONProcessingError.JSONParsingError
            }
        }
        return objects
    }
    
    func objectForRow(row:NSInteger)->BookModel{
        return self.bookModels()[row]
    }
    
    func bookTags()->Array<String>{
        for key in (self.books.keys) {
            self.tags.append(key)
        }
        return self.tags
    }
   
    func numberOfRowInSection(section:Int)->Int{
        return self.books[self.tags[section]]!.count
    }
    
    func viewModelFromModel(bookModels:[BookModel])-> Dictionary<String,Array<BookModel>>
    {
        var dic:Dictionary<String,Array<BookModel>> = [:]
        for b in bookModels {
            for tag in (b.bookTags?.componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ",")))! {
                if var books = dic[tag] {
                    books.append(b)
                    // El problema es que se pasa como valor y no como referencia
                    dic[tag] = books
                } else {
                    var books = Array<BookModel>()
                    books.append(b)
                    dic[tag] = books
                    
                    //NSSset para ordenar y que no se repitan los tags
                    
                }
            }
        }
        
        return dic;
    }
    
}


