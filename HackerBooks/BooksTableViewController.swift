//
//  BooksTableViewController.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BooksTableViewController: UITableViewController,BookViewInputProtocol{
    
    var presenter:BookPresenter?
    var books:Dictionary<String,Array<BookModel>> = [:]

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return (self.presenter?.numberOfSections())!
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
//        return self.presenter?.titleForSection(section)
        return "section\(section)"
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.presenter?.numberOfRowInSection(section))!
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        return (self.presenter?.cellForBook(tableView, indexPath: indexPath, books: self.books))!
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
         return (self.presenter?.heightForRow())!
    }

    func giveMeListFromBooks(books:Dictionary<String,Array<BookModel>>){
        self.books = books
        self.tableView.reloadData()
    }
    
}
