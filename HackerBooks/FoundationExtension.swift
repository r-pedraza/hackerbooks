//
//  FoundationExtension.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 30/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import Foundation


extension NSBundle{

    func URLForResource(fileName:String)->NSURL?{
    
        let tokens = fileName.componentsSeparatedByString(".")
    
        return self.URLForResource(tokens.first,withExtension:tokens.last )
    }

}