//
//  BookingCell.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 30/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

class BookCell: UITableViewCell {
    
    @IBOutlet weak var authorsLabel: UILabel!

    @IBOutlet weak var bookImage: UIImageView!

    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    
    func identifierCell()->String{
        return "cell"
        }
    
    func nibName()->String{
    
        return "BookCell"
    }
    
    func heigthForRow()->Float32{
    
        return 200.0
    }
    
    @IBAction func bookDownload(sender: UIButton) {
        
        print("me has pulsado")
    }
    
    func rowWithBookModel(book:BookModel,tableView:UITableView)->BookCell{
    
        var cell: BookCell! = tableView.dequeueReusableCellWithIdentifier(self.identifierCell()) as? BookCell
        
        if cell == nil {
            tableView.registerNib(UINib(nibName: BookCell().nibName(), bundle: nil), forCellReuseIdentifier: BookCell().identifierCell())
            cell = tableView.dequeueReusableCellWithIdentifier(BookCell().identifierCell()) as? BookCell
        }
 
        cell.authorsLabel.text = book.bookAuthor
        cell.titleLabel.text   = book.bookTitle
        cell.tagsLabel.text    = book.bookTags
        BooksAsyncImage().asyncloadBookImage(book){ image in
             cell.bookImage.image = image
        }
        
        return cell
    }
}