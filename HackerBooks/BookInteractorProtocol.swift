//
//  BookInteractorProtocolOutput.swift
//  HackerBooks
//
//  Created by Raúl Pedraza on 7/12/15.
//  Copyright © 2015 Raúl Pedraza. All rights reserved.
//

import UIKit

protocol BookInteractorOutput{

    func numberOfRowInSection(section:Int)->Int
}

protocol BookInteractorInput{
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage
}